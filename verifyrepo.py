#!/usr/bin/env python3

import sys, subprocess, os.path, re

#=========================================================
# Local customization section
clone_info = {
    'username': 'readonlyscriptuser',
    'api_token': 'mGCDkwHGMkUvKEQTMbGq',
}
# Full path to git command
git_cmd = '/usr/bin/git'
# Timeout of clone command (in seconds)
clone_timeout = 2
# Path to git server (without the repository name).
# Must be https, and include placeholders username and api_token for substitution from above
server_path = 'https://{username}:{api_token}@git.ucsc.edu/'.format (**clone_info)
#=========================================================

#=========================================================
# Shouldn't need to modify anything past this point
#=========================================================
from tempfile import TemporaryDirectory

class proc_return:
    def __init__ (self, returncode, stdout, stderr=None):
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr

def run_command (cmd_string, timeout = 2):
    try:
        r = subprocess.run (cmd_string.split (), stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=timeout)
        return proc_return (r.returncode, str (r.stdout, 'utf-8'), str (r.stderr, 'utf-8'))
    except subprocess.TimeoutExpired:
        return proc_return (1, '', '')

    try:
        output = subprocess.call_output (cmd_string.split(), stderr=subprocess.STDOUT, timeout=timeout)
    except subprocess.TimeoutExpired as e:
        return proc_return (1, e.output, '')
    except subprocess.CalledProcessError as e:
        return proc_return (e.returncode, e.output, '')
    return proc_return (0, output, '')

def verify_repo (repo, script_to_run, commit_id = None, script_timeout = 5):
    '''
    Clones a repository (repo) and runs script_to_run on the cloned repository.
    script_to_run must be a string that includes any necessary arguments, with
    {repo_dir} acting as a placeholder for the actual repo directory.
    The executable must be an absolute path that's valid on the local
    system.  script_timeout is the number of seconds after which the script
    will be timed out.

    Returns a tuple <return_code, command_stdout, command_stderr>

    If the repo can't be cloned, the return code and return string will be from
    the clone operation.  If the clone is successful, the return code and string
    will be from the script that's run.

    Example call:
    verify_repo ('cmpe012/winter18/cruzid', '/bin/ls -R {repo_dir}')    
    '''
    with TemporaryDirectory() as tmpdir:
        repo_dir = os.path.join (tmpdir, 'clonedrepo')
        clone_cmd = '{0} clone {1} {2}'.format (git_cmd, server_path + repo, repo_dir)
        try:
            r = run_command (clone_cmd, clone_timeout)
            if r.returncode != 0:
                raise OSError ()
            if commit_id:
                if not re.fullmatch (r'[a-fA-F0-9]{40}', commit_id):
                    raise ValueError ()
                checkout_cmd = '{0} -C {1} checkout {2}'.format (git_cmd, repo_dir, commit_id)
                r = run_command (checkout_cmd, clone_timeout)
                if r.returncode != 0:
                    raise OSError ()
            scr = script_to_run.format (repo_dir=repo_dir)
            r = run_command (scr, script_timeout)
        except ValueError:
            return (1, '', "{0} isn't a valid commit ID!\nCommit IDs are 40 hex characters.".format (commit_id))
    return (r.returncode, r.stdout, r.stderr)

if __name__ == '__main__':
    # Test by listing a test repo
    for commit_id in ('40660620383d6b891f805018202bf7ce50f15f83', None, '9967f92ac2bf2f666eef419716fe9869c4e8d777',
                      'foo', '40660620383d6b891f805018202bf7ce50f15f8'):
        (ret, stdout, stderr) = verify_repo ('classes/testing/elm', '/bin/ls -R {repo_dir}', commit_id)
        print ('================== {0} ====================\nret={1}'.format (commit_id, ret))
        print ('-- stdout --\n{0}\n-- stderr --\n{1}\n'.format (stdout, stderr))

