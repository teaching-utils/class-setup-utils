#!/usr/bin/env bash
#
# Modify file(s) in existing git repositories.
#
# NOTE: requires write permission for the repositories in question.
#       The best way to do this is to use the following URL:
#       https://oauth2:ACCESS_TOKEN@git.ucsc.edu/path/to/group
#       Substitute an ACCESS_TOKEN that allows writes into the URL.
#
# USAGE: patchrepos.sh BASE_URL COMMIT_MSG PATCH [PATCH...]
#        BASE_URL:  The complete URL to clone a repository, without the (trailing) repo name.
#        COMMIT_MSG:The commit message to use after patching.
#        PATCH:     The file name of the git patch to apply. Note that relative names must
#                   be relative to the tmprepo subdirectory below the directory in which the
#                   command is run.
 
base_url=$1
commit_msg=$2
shift 2
patches=$*

while read -r line; do
    reponame=$line
    full_url=$base_url/$reponame
    git clone -q $full_url tmprepo
    if [ $? ]; then
        echo "FATAL: couldn't clone $full_url - exiting!"
        exit 1
    fi
    cd tmprepo
    docommit=2
    for p in $patches; do
        git apply -q $p
        if [ $? ] ; then
            echo "!!! $reponame: patch failed - no changes pushed!"
            docommit=0
            break
        fi
    done
    if [ $docommit -eq 2 ]; then
        git commit -q -am "$commit_msg"
        git push -q
        echo "   $reponame: patch succeeded!"
    fi
    cd ..
    /bin/rm -fr tmprepo
done
