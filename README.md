# Overview

This repo contains utilities and instructions for setting up class repositories on `git.ucsc.edu` for UCSC classes.

# Using the tools

Please see the [wiki](https://git.ucsc.edu/teaching-utils/class-setup-utils/-/wikis/home) for
complete details and instructions.
